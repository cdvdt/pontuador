import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Classe que implementa os dialogos.
class CustomDialog {

  /// Mostra um inputQuery na tela.
  static Future<void> inputQuery(BuildContext context,String title, String value,
  {String? caption, String? hint, required void Function(String value) handler, TextInputType? textType = TextInputType.text, bool onlyNumbers = false, int? maxLength}) {
    final TextEditingController controller = TextEditingController(text: value);

    final Widget body = Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
      if (caption != null) Text(caption, style: Theme.of(context).textTheme.subtitle2),
      TextFormField(
          maxLength: maxLength,
          controller: controller,
          style: Theme.of(context).textTheme.bodyText1,
          decoration: InputDecoration(
            hintText: hint ?? '',
          ),
        keyboardType: textType,
        inputFormatters: <TextInputFormatter>[
          if (onlyNumbers) FilteringTextInputFormatter.digitsOnly
        ],
      )
    ]);

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title, style: Theme.of(context).textTheme.headline3),
          content: body,
          actions: <Widget>[
            TextButton(
              child: Text('Cancel', style: Theme.of(context).textTheme.bodyText2,),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Submit', style: Theme.of(context).textTheme.bodyText2),
              onPressed: () {
                handler(controller.text);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  /// Mostra uma pergunta de sim ou não.
  static Future<bool?> question(BuildContext context,{required String title,
      required void Function(bool value) handler}) {

    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          //title: Text(titulo),
          content: Text(title, style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal),),
          actions: <Widget>[
            TextButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop(true);
                handler(true);
              },
            ),
            TextButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop(false);
                handler(false);
              },
            ),
          ],
        );
      },
    );
  }

  static showMessage(BuildContext context, String message){
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(message),
          duration: const Duration(seconds: 3)
        ));
  }

  static showError(BuildContext context, String message){
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(message),
          duration: const Duration(seconds: 3),
          backgroundColor: Colors.red,
        ));
  }

  Future<void> modalBottomSheet({required BuildContext context, required Widget child}) async{
    await showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: child
        );
      },
    );
  }

  /*
  static Future<void> inputDouble(BuildContext context, String titulo, double valor,
      {String? descricao, String? hint, required void handler(double valor)}) {
    var controller = MoneyMaskedTextController(initialValue: valor);

    final Widget corpo = Column(mainAxisSize: MainAxisSize.min, children: [
      if (descricao != null) Text(descricao),
      TextField(
        controller: controller,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          hintText: hint ?? '',
        ),
        keyboardType: TextInputType.number,
      )
    ]);

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(titulo),
          content: corpo,
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Submit'),
              onPressed: () {
                handler(controller.numberValue);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  */


}